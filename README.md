# About me...
Javier Martinez Alcantara, Javi.

[LinkedIn](https://www.linkedin.com/in/javier-martinez-alcantara/)

[Some info](https://jmaralc.github.io/)

# Snake Eyes

The idea of this project is to present an example of websockets in python within FastAPI framework.

# Motivation

Despite websocket is a mature technology, sometimes is difficult to find good examples in the web that present the potential of the technology. This repository tries to present a real application where both sides of the application (server and client) can be reached just putting in place the python project.

[Some related slides](https://docs.google.com/presentation/d/1yhjhHkYjHkCpsJglMgKIwt8AqLRwiTsFJIIdYH_fSb8/edit?usp=sharing)

# Setup

For this talk you will need python 3.8 and pip installed in your computer. We will use pipenv for installing the packages and keeping the virtual environment. So I recomend you to install it with pip:

```shell
pip install --user --upgrade pipenv
```
Further information about installation [here](https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv)

# Global Goal

Spread the knowledge of python, FastAPI and websockets.

# Structure of the repository/project

The static folder contains the html, javascript and css code needed in the client side. There is also a folder with assets (images mainly).
Despite that during the live coding session we coded everything within a single file I preferred to split a bit the code:

**Server** where the main definition of the FastAPI based application is done. We defined here the endpoints and the logic to handle websockets.

**Crupier** the class that manage the set of game tables and the users.

**Logger** as simple python script to generate a logger.


# How to run it
First set in place pipenv with:

```shell
pipenv install
```

To start the server:
```shell
pipenv run uvicorn server:app --workers 1 --reload
```

# Special thanks to..

For the idea:
https://dados.tr4ck.net/#!/

For the opportunity:
https://codurance.com/

For the nice framework:
https://fastapi.tiangolo.com/