// Definition of the dies we are going to have in our application
const dice = ['d4', 'd6', 'd8', 'd10', 'd12', 'd20']

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

// Generating an object with the different dice as fields
const userStats = dice.reduce((dice, die) => {
    dice[die] = [];
    return dice;
}, {})
console.log(userStats);

// Function to generate the statistics of the rolls
// Mainly related to how Plotly works
const generateRollStats = function (dieType) {
    console.log("Generating stats for " + dieType)
    console.log(parseInt(dieType.split("d")[1]))
    if (userStats[dieType].length > 0) {
        var trace = {
            type: 'histogram',
            x: userStats[dieType],
            autobinx: false,
            xbins: {
                end: parseInt(dieType.split("d")[1]),
                size: 1,
                start: -0.5
            },
            marker: {
                color: '#099cdb',
            },
            opacity: 0.5,
        };
    
        console.log(trace)
    
        var layout = {
            xaxis: {
                automargin: true,
                ticks: 'insioutsidede',
                tickwidth: 2,
                ticklen: 5,
                dividercolor: '#099cdb',
                tickcolor: '#099cdb',
                // range: [0, parseInt(dieType.split("d")[1])+0.5]
                showgrid: true,
                zeroline: false,
                showline: true,
                gridcolor: '#addaed',
                gridwidth: 2,
                zerolinecolor: '#099cdb',
                zerolinewidth: 4,
                linecolor: '#099cdb',
                linewidth: 2
            },
            yaxis: {
                showgrid: true,
                zeroline: false,
                showline: true,
                gridcolor: '#addaed',
                gridwidth: 2,
                linewidth: 0
            },
            autosize: true,
            // width: 175,
            height: 200,
            margin: {
                l: 0,
                r: 0,
                b: 0,
                t: 0,
                pad: 6
            },
            paper_bgcolor: '#e9eef0',
            plot_bgcolor: '#e9eef0'
        };
    
        var data = [trace];
    
        Plotly.newPlot('stats_' + dieType, data, layout, {displayModeBar: false});
    }

}

// Function to generate a dieroller interface given a type of die
const generateDieRoller = function(dieType){
    let diceField = document.getElementById('user_dice');
    var dieDiv = document.createElement("div");
    dieDiv.id = "div"+dieType;
    dieDiv.classList.add("slidecontainer");

    var dieLabel = document.createElement("label")
    dieLabel.classList.add("dieLabel");
    dieLabel.for = dieType
    dieLabel.innerHTML = dieType

    var dieValue = document.createElement("input");
    dieValue.id = "value_" + dieType;
    dieValue.classList.add("quantity");
    dieValue.type = 'number';
    dieValue.value = 0
    dieValue.maxLength = "2"
    dieValue.max = 20
    dieValue.min = 0
    dieValue.size = "2"

    var dieSlider = document.createElement("input");
    dieSlider.id = dieType;
    dieSlider.classList.add("slider");
    dieSlider.type = 'range';
    dieSlider.min = 0;
    dieSlider.max = 20;
    dieSlider.value = 0;
    dieSlider.step = 1;
    dieSlider.oninput = function (){return dieValue.value = this.value}
    dieValue.oninput = function (){
        if(this.value>20){this.value =20}
        if(this.value<0){this.value =0}
        return dieSlider.value = this.value
    }


    // dieSlider.setAttribute("id", dieType);
    dieDiv.appendChild(dieValue)
    dieDiv.appendChild(dieLabel)
    dieDiv.appendChild(dieSlider)
    diceField.appendChild(dieDiv)

    var dieStats = document.createElement("div");
    dieStats.id = "stats_" + dieType;
    dieStats.classList.add("plotStats");
    dieDiv.appendChild(dieStats);
    generateRollStats(dieType);
}
// For each die in our applicacion we will have a dieRoller
dice.forEach(die => generateDieRoller(die))

// Function to generate a player Field where the results of the rolls
// will be shown.
const createNewPlayerField = function (user) {
    let players = document.getElementById('players');

    var divUser = document.createElement("div");
    divUser.id = user;
    divUser.classList.add("playerField");

    var divUserName = document.createElement("div");
    divUserName.id = "name_" + user;

    let tesxtUserName = document.createTextNode("Player: " + user);
    divUserName.appendChild(tesxtUserName);

    var timestampUser = document.createElement("div");
    timestampUser.classList.add("playerTimestamp");
    timestampUser.id = "timestamp_" + user;

    let textTimestampUser = document.createTextNode("Nothing happened");
    timestampUser.appendChild(textTimestampUser);

    var divUserResults= document.createElement("div");
    divUserResults.id = "results_"+ user;

    divUser.appendChild(divUserName);
    divUser.appendChild(timestampUser);
    divUser.appendChild(divUserResults);
    players.appendChild(divUser);
}

// Function to append results
const appendRollResults = async function (data) {
    let user = data.user_name;
    let rollResults = data.rolls;
    document.getElementById("timestamp_" + user).innerHTML = "Last roll: " + new Date().toLocaleString();
    console.log(data)
    console.log(user)
    console.log(rollResults)
    let userResultField = document.getElementById("results_"+user);
    userResultField.innerHTML = '';
    for (let [key, values] of Object.entries(rollResults)) {
        console.log(`${key}: ${values}`);

        if (user === myUserName) {
            userStats[key].push(...values)
            console.log(userStats)
            generateRollStats(key)
        }

        if (values.length > 0){
            let resultDieField = document.createElement("div");
            resultDieField.classList.add("resultDieField");

            let totalResultField = document.createElement("div");
            totalResultField.classList.add("resultField");
            // totalResultField.style="justify-content: center;";

            let totalResultLabel = document.createElement("label");
            totalResultLabel.classList.add("labelResult");
            totalResultLabel.innerHTML = "Total:" + values.reduce(function(a, b){
                return a + b;
                }, 0) + "=";

            totalResultField.appendChild(totalResultLabel)
            resultDieField.appendChild(totalResultField)

            values.sort(function(a, b){return a - b});
            let critics = values.filter(value => value==20)
            if(critics.length >0){await showCrit(1.5)}
            values.forEach(value =>{
                console.log(value)
                if(value == null){return}
    
                let resultField = document.createElement("div");
                resultField.classList.add("resultField");
                // resultField.style="float: left;";

                let resultImage = document.createElement("img");
                resultImage.src = "./assets/"+key+"-resultado.png";
                resultImage.style = "width:40px;";
    
                let resultLabel = document.createElement("label");
                resultLabel.classList.add("labelResult");
                resultLabel.innerHTML = value
    
                resultField.appendChild(resultImage)
                resultField.appendChild(resultLabel)
                resultDieField.appendChild(resultField)
            })
            userResultField.appendChild(resultDieField);
        }


    }
}

// The code below is just to adjust the plots of the results when resizing the window
window.onresize = function(){
    console.log("RESIZING")
    dice.forEach(die => generateRollStats(die))
}

var globalLayer = document.getElementsByClassName("globalLayer")[0]


console.log(globalLayer)
console.log(globalLayer.style)

globalLayer.addEventListener("webkitAnimationEnd", hiddeCourtains);
globalLayer.addEventListener("animationend", hiddeCourtains);

async function hiddeCourtains(showTime) {
    console.log("Ocultando")
    console.log(globalLayer)
    this.classList.remove("appearing")
    this.classList.remove("bgflashing");

    this.innerHTML = '';
    this.style.visibility = "hidden";
}

async function textDissapear(showTime) {
    this.parentNode.removeChild(this)
}

async function showCrit(showTime){
    var globalLayer = document.getElementsByClassName("globalLayer")[0];

    var specialText = document.createElement("h1");
    specialText.classList.add("effectText");
    specialText.classList.add("textflashing");

    specialText.innerHTML = "Critical hit!!"

    document.body.appendChild(specialText)
    specialText.addEventListener("animationend", textDissapear);

    globalLayer.style.visibility = "visible";
    globalLayer.classList.add("bgflashing");
    globalLayer.addEventListener("animationend", hiddeCourtains,showTime);
    
}
// Initial parameters that are within the client session
// These are taken from the form at index.html
let params = (new URL(location.href)).searchParams; 
let myUserName = params.get("user_name")
let current_users = []
let myTableName = params.get("table")
console.log(myUserName)
console.log(myTableName)

// Definition of theroutine for requesting information about the table
const request = async () => {
    // Making the call to the dice_table endpoint to register ourselves in the 
    // management system of tables
    const response = await fetch("http://127.0.0.1:8000/dice_table", {
        method:"POST",
        body: JSON.stringify({
            user_name: myUserName,
            table_name: myTableName
        })
    });

    const json = await response.json();
    return json
};

const register = async () => {
    // Requesting the information about the table
    let response = await request();
    console.log(response)

    // This is the way a websocket client connect with a websocket server
    var websocket = new WebSocket(
        `ws://localhost:8000/table/${response.tableName}`
    );
    
    console.log(websocket.readyState)

    // When the websocket is open the first request we send is one with:
    // Action == register
    // my current user name
    websocket.onopen = ()=>{
        websocket.send(JSON.stringify({
            action: "register",
            user_name: myUserName
        }));
    }

    return websocket
}

// Definition of the function that will take care of given a websocket 
// set the diferent actions we will need to define
const define_events = async (websocket) => {
    // We await for a valid websocket
    var websocket = await register()

    // Append the onclick handler to the roll button.
    // It is a function that will submmit a ROLL action through
    // the websocket.
    let rollButton =document.getElementById('rollButton');
    rollButton.onclick = function (event) {
        let roll_dies = dice.reduce((dice, die) => {
            dice[die] = document.getElementById(die).value;
            document.getElementById("value_" + die).value = 0
            document.getElementById("value_" + die).dispatchEvent(
                new Event("input",{})
            )
            return dice;
        }, {})
        console.log(roll_dies)

        websocket.send(
            JSON.stringify(
                {
                    action:"roll",
                    user_name: myUserName,
                    dice: roll_dies
                }
            )
        );
    };

    // Definition of the websocket message on message
    websocket.onmessage = async function (event) {
        console.log(event)
        data = JSON.parse(event.data)
        console.log(typeof(data))
        console.log(data)

        switch (data.type) {
            // If we received a roll from the websocket we draw it with the appendRollResults function
            case 'roll':
                console.log("ROLL")
                await appendRollResults(data)
                break;
            // If we receibe a regiser event we check the user to draw it properly in our client (browser)
            case 'register':
                console.log("REGISTER")
                console.log(data)
                console.log(data.users)
                console.log(myUserName)

                // Just take all the users but my user
                let filteredUsers = data.users.filter(user =>user!==myUserName)
                // My user will be always in the top position. The rest will appear based on arrival time
                filteredUsers.splice(0,0,myUserName)

                // Check whether the user exists and if not, create a field for them
                filteredUsers.map(user => {
                    let user_exist = document.getElementById(user)
                    if( user_exist == null){
                        console.log(`Creating the field for player ${user}`)
                        createNewPlayerField(user)
                        current_users.push(user)
                    }
                    else{
                        console.log(`The player ${user} is already registered`)
                    }
                })

                // In case a user leave the table we need to delete it's player field
                console.log("Checking the current users")
                console.log(current_users)
                current_users.map(user => {
                    console.log(user)
                    if(!filteredUsers.includes(user)){
                        console.log(`Deleting user ${user}`);
                        var element = document.getElementById(user);
                        element.parentNode.removeChild(element)
                    }
                })
                break;
            default:
                console.error("unsupported event", data);
        }
    };
};

// Calling the definition of events when the page loads.
define_events();