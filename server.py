import asyncio
import uuid

from fastapi import FastAPI, WebSocket
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
from starlette.websockets import WebSocket, WebSocketDisconnect

from crupier import Crupier
from logger import logger

app = FastAPI()
crupier = Crupier()

origins = [
    "http://localhost",
    "http://localhost:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Body(BaseModel):
    table_name: str
    user_name: str

app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/", response_class=HTMLResponse)
async def redirect():
    html_content = """
    <html>
    <head>
        <meta http-equiv="refresh" content="0;url=http://localhost:8000/static/index.html" />
        <link rel="icon" href="./assets/icon.png" type="image/png" sizes="16x16">
        <title></title>
    </head>
    <body></body>
    </html>
    """
    return HTMLResponse(content=html_content, status_code=200)


@app.post("/dice_table")
async def dice_table(body: Body):
    logger.info("Dice table endpoint")
    table = await crupier.get_table(body.table_name)
    if table is None:
        logger.info("Table does not exist. Creating it..")
        await crupier.create_table(body.table_name)
    else:
        logger.info("Table exist. Getting id")

    return {"tableName": body.table_name}


@app.websocket("/table/{table_name}")
async def websocket_endpoint(
    websocket: WebSocket, table_name: str,
):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_json()
            logger.info(f"Data received: {data}")

            ws_action = data.get("action")

            if ws_action == "register":
                logger.info("REGISTERING")

                table = await crupier.get_table(table_name)
                users = table.get("users")
                users.append(
                    {
                        "user": websocket,
                        "user_name": data.get("user_name"),
                        "rolls": {},
                    }
                )
                table["users"] = users

                await crupier.set_table(table_name, table)
                logger.info(table)

                payload = {
                    "type": "register",
                    "users": [user.get("user_name") for user in table["users"]],
                }

                for user in users:
                    await user.get("user").send_json(payload)

            elif ws_action == "roll":
                logger.info("ROLLING")
                dice_results = await crupier.roll(data.get("dice"))
                payload = {
                    "type": "roll",
                    "user_name": data.get("user_name"),
                    "rolls": dice_results,
                }
                logger.info(payload)
                users = table.get("users")
                for user in users:
                    await user.get("user").send_json(payload)

            else:
                logger.warning(f"Unhandled action {ws_action}")

    except WebSocketDisconnect:
        logger.info("Disconnecting")
        table = await crupier.get_table(table_name)
        users = table.get("users")

        element_to_remove = None
        for index_p, user in enumerate(users):
            if user.get("user") == websocket:
                element_to_remove = index_p
        users.pop(element_to_remove)
        table["users"] = users

        await crupier.set_table(table_name, table)

        payload = {
            "type": "register",
            "users": [user.get("user_name") for user in table["users"]],
        }

        for user in users:
            await user.get("user").send_json(payload)



